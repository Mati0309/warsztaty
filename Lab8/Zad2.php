<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Zad2</title>
</head>
<body>
<form method="post">
    Wpisz ciąg tekstowy(Słowa oddzielone spacją):<br>
    <input type="text" name="tekst"><br>
    Wybierz operację:<br>
    <select name="wybor">
        <option value="1">Ekstrakcja unikalnych słów</option>
        <option value="2">Sortowanie rosnąco</option>
        <option value="3">Sortowanie malejąco</option>
    </select><br><br>
    <input type="submit" value="Analizuj"><br>
    <?php
    if (isset($_POST['tekst']) && isset($_POST['wybor'])) {
        $tekst = $_POST['tekst'];
        $wybor = $_POST['wybor'];
        $array = explode(" ", $tekst);
        switch ($wybor) {
            case 1:
                $array2 = array_count_values($array);

                echo "<table>";
                echo "<tr>";
                echo "<th>Słowo</th>";
                echo "<th>Częstotliwość</th>";
                echo "</tr>";
                foreach ($array2 as $key => $value) {
                    echo "<tr>";
                    echo "<td>$key</td>";
                    echo "<td>$value</td>";
                    echo "</tr>";
                }
                echo "</table>";

                break;
            case 2:
                sort($array);
                echo implode(" ", $array);
                break;

            case 3:
                rsort($array);
                echo implode(" ", $array);
                break;
        }
    }
    ?>
</form>

</body>
</html>
