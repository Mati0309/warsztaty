<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Zad1</title>
</head>
<body>
<form method="post">
    Wpisz tekst<br>
    <input type="text" name="tekst"><br>
    Wybierz operację:<br>
    <select name="wybor">
        <option value="1">Odwrócenie</option>
        <option value="2">Zamiana na duże</option>
        <option value="3">Zamiana na małe</option>
        <option value="4">Liczenie znaków</option>
        <option value="5">Usunięcie znaków białych</option>
    </select><br><br>
    <input type="submit" value="Wykonaj"><br>
    <?php

    if (isset($_POST['tekst']) && isset($_POST['wybor'])) {
        $tekst = $_POST['tekst'];
        $wybor = $_POST['wybor'];

        echo "Wynik: ";
        switch ($wybor) {
            case 1:
                echo strrev($tekst);
                break;
            case 2:
                echo strtoupper($tekst);
                break;
            case 3:
                echo strtolower($tekst);
                break;
            case 4:
                echo strlen($tekst);
                break;
            case 5:
                echo trim($tekst);
                break;
        }
    }
    ?>
</form>

</body>
</html>
