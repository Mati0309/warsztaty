<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Zad3</title>
</head>
<body>
<form method="post">
    Wpisz tekst<br>
    <input type="text" name="tekst"><br>
    Regex<br>

    <input type="text" name="preg"><br>
    Wybierz operację:<br>
    <select name="wybor">
        <option value="1" >Występowanie wzorca</option>
        <option value="2">Zamiana słów</option>
        <option value="3">Dopasowanie</option>
    </select><br><br>
    <?php
        echo "Zamiana <br>";
        echo "<input type='text' name='zamiana'>"; ?><br><br>
    <input type="submit" value="Wykonaj"><br>
    <?php
    if (isset($_POST['tekst']) && isset($_POST['wybor']) && isset($_POST['preg'])) {
        $tekst = $_POST['tekst'];
        $wybor = $_POST['wybor'];
        $regex = $_POST['preg'];
        $zamiana = $_POST['zamiana'];
        switch ($wybor) {
            case 1:
                preg_match_all($regex,$tekst,$tekst2,PREG_OFFSET_CAPTURE);
                var_export($tekst2);
                break;
            case 2:
                echo preg_replace($regex, $zamiana, $tekst);
                break;
            case 3:
                preg_match($regex,$tekst);
                break;
        }
    }
    ?>
</form>

</body>
</html>

