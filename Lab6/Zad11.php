<html>
<head>
    <title>Zad11</title>
    <link href="Zad11.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php
function ToFahr($a)
{
    $b = ($a*1.8) + 32;
    return $b;
}

function ToCel($a)
{
    $b = ($a-32)/1.8;
    return $b;
}
?>
<div class="container">
    <div>
        <table>
     <tr class = "s">
         <th>Celcius</th>
         <th class="checked">Fahrenheit</th>
     </tr>
        <tr>
        <?php
        $a = 40;
        $b = ToFahr($a);
        echo "<td>$a</td>";
        echo "<td class='checked'>$b</td>";
        ?>
        </tr>
            <tr>
                <?php
                $a = 60;
                $b = ToFahr($a);
                echo "<td>$a</td>";
                echo "<td class='checked'>$b</td>";
                ?>
            </tr>
            <tr>
                <?php
                $a = 50;
                $b = ToFahr($a);
                echo "<td>$a</td>";
                echo "<td class='checked'>$b</td>";
                ?>
            </tr>
        </table>
    </div>
    <div>
        <table>
            <tr class="s">
                <th>Fahrenheit</th>
                <th>Celcius</th>
            </tr>
            <tr>
                <?php
                $a = 120;
                $b = round(ToCel($a),2);
                echo "<td>$a</td>";
                echo "<td>$b</td>";
                ?>
            </tr>
            <tr>
                <?php
                $a = 110;
                $b = round(ToCel($a),2);

                echo "<td>$a</td>";
                echo "<td>$b</td>";
                ?>
            </tr>
            <tr>
                <?php
                $a = 50;
                $b = ToCel($a);
                echo "<td>$a</td>";
                echo "<td>$b</td>";
                ?>
            </tr>
        </table>
    </div>
</div>



</body>
</html>
