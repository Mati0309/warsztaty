<html>
<head>
    <title>Zad3</title>
</head>
<body>

<form method="post">
    <input type="text" name="age" placeholder="d-m-Y">
    <select name="time">
        <option value="1">Tokyo,Japan</option>
        <option value="2">Warsaw,Poland</option>
        <option value="3">London,Great Britain</option>
        <option value="4">Honolulu,Hawaii</option>
    </select>
    <input type="submit" value="wyślij">
</form>
<?php
if(isset($_POST['time']) && isset($_POST['age'])) {


    $time = $_POST['time'];
    switch ($time) {
        case 1:
            date_default_timezone_set("Asia/Tokyo");
            echo date("H:i:s");
            break;
        case 2:
            date_default_timezone_set("Europe/Warsaw");
            echo date("H:i:s");
            break;
        case 3:
            date_default_timezone_set("Europe/London");
            echo date("H:i:s");
            break;
        case 4:
            date_default_timezone_set("Pacific/Honolulu");
            echo date("H:i:s");
            break;


    }
    echo "<br>";
    $dateOfBirth = $_POST['age'];
    $today = date('d-m-Y');
    $diff = date_diff(date_create($dateOfBirth), date_create($today));
    echo "Wiek " . $diff->format("%y");
}
?>

<form method="post">
    <input type="text" name="first" placeholder="d-m-Y">
    <input type="text" name="last" placeholder="d-m-Y">
    <input type="submit" value="wyślij">
</form>
<?php
if(isset($_POST['first']) && isset($_POST['last'])) {


    $first = strtotime($_POST['first']);
    $last = strtotime($_POST['last']);
    $licznik = 0;
    for ($i = $first; $i <= $last; $i = strtotime("+1 day", $i)) {
        $day = date("w", $i);
        if ($day != 0 && $day != 6) {
            $licznik++;
        }
    }
    echo "Liczba dni roboczych wynosi: " . $licznik;
}


?>
</body>

</html>
