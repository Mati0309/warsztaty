<?php
session_start();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $_SESSION['bgColor'] = $_POST['bgColor'];
    $_SESSION['textColor'] = $_POST['textColor'];
    header('Location: Zad3a.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Potwierdzenie Ustawień</title>
</head>
<body>
<h1>Twoje ustawienia zostały zapisane.</h1>
<p>Czy potwierdzasz ustawienia?</p>
<?php
echo "Tło :" . $_SESSION['bgColor'] . " zaś kolor czcionki: " . $_SESSION['textColor'];
echo "<br>";
?>
<a href="Zad3.php">Cofnij do ustawień</a>
<a href="Zad3b.php">Przejdź do treści</a>
</body>
</html>

