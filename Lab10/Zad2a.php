<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $bgColor = $_POST['bgColor'];
    $textColor = $_POST['textColor'];
    setcookie('bgColor', $bgColor);
    setcookie('textColor', $textColor);
    header('Location: Zad2a.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Potwierdzenie Ustawień</title>
</head>
<body>
<h1>Twoje ustawienia zostały zapisane.</h1>
<p>Czy potwierdzasz ustawienia?</p>
<?php
echo "Tło :" . $_COOKIE['bgColor'] . " zaś kolor czcionki: " . $_COOKIE['textColor'];
echo "<br>";
?>
<a href="Zad2.php">Cofnij do ustawień</a>
<a href="Zad2b.php">Przejdź do treści</a>
</body>
</html>
