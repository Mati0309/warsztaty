<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Ustawienia Preferencji</title>
</head>
<body>
<h1>Ustawienia Preferencji</h1>
<form action="Zad2a.php" method="POST">
    <label for="bgColor">Kolor tła:</label>
    <select id="bgColor" name="bgColor">
        <option value="red">Czerwony</option>
        <option value="lightgray">Jasnoszary</option>
        <option value="lightblue">Jasnoniebieski</option>
    </select>
    <br><br>
    <label for="textColor">Kolor tekstu:</label>
    <select id="textColor" name="textColor">
        <option value="yellow">Zółty</option>
        <option value="blue">Niebieski</option>
        <option value="red">Czerwony</option>
    </select>
    <br><br>
    <button type="submit">Zapisz ustawienia</button>
</form>
</body>
</html>


