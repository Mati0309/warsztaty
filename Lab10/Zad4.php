<?php
$passwords = ['user1' => 'password1',
    'user2' => 'password2',
    'user3' => 'password3',
    'user4' => 'password4'];

$username = $_SERVER['PHP_AUTH_USER'];
$password = $_SERVER['PHP_AUTH_PW'];

if (!isset($username) || !isset($password) || isset($_POST['logout'])) {
    header('WWW-Authenticate: Basic realm="Kapibara');
}
?>


<html>
<head>
    <title>Sklep</title>
</head>
<body>
<h1>Sklep</h1>
<form method="post">
    Produkt 1
    <input type="submit" name="Produkt1">
    <?php
    if(isset($_POST['Produkt1']))
    {
        if(isset($_COOKIE['koszyk'.$username])){
            $koszyk = $_COOKIE['koszyk'.$username];
            $Produkty = explode(",", $koszyk);
            if(!in_array("Produkt1",$Produkty))
            {
                array_push($Produkty,"Produkt1");
                $nowyKoszyk = implode(",",$Produkty);
                setcookie("koszyk".$username,$nowyKoszyk);
            }
        }else{
            setcookie("koszyk".$username,"Produkt1");
        }
    }
    ?>
</form>
<form method="post">
    Produkt 2
    <input type="submit" name="Produkt2">
    <?php
    if(isset($_POST['Produkt2']))
    {
        if(isset($_COOKIE['koszyk'.$username])){
            $koszyk = $_COOKIE['koszyk'.$username];
            $Produkty = explode(",", $koszyk);
            if(!in_array("Produkt2",$Produkty))
            {
                array_push($Produkty,"Produkt2");
                $nowyKoszyk = implode(",",$Produkty);
                setcookie("koszyk".$username,$nowyKoszyk);
            }
        }else{
            setcookie("koszyk".$username,"Produkt2");
        }
    }
    ?>
</form>

<form method="post">
    Produkt 3
    <input type="submit" name="Produkt3">
    <?php
    if(isset($_POST['Produkt3']))
    {
        if(isset($_COOKIE['koszyk'.$username])){
            $koszyk = $_COOKIE['koszyk'.$username];
            $Produkty = explode(",", $koszyk);
            if(!in_array("Produkt3",$Produkty))
            {
                array_push($Produkty,"Produkt3");
                $nowyKoszyk = implode(",",$Produkty);
                setcookie("koszyk".$username,$nowyKoszyk);
            }
        }else{
            setcookie("koszyk".$username,"Produkt3");
        }
    }
    ?>
</form>

<form method="post">
    Produkt 4
    <input type="submit" name="Produkt4">
    <?php
    if(isset($_POST['Produkt4']))
    {
        if(isset($_COOKIE['koszyk'.$username])){
            $koszyk = $_COOKIE['koszyk'.$username];
            $Produkty = explode(",", $koszyk);
            if(!in_array("Produkt4",$Produkty))
            {
                array_push($Produkty,"Produkt4");
                $nowyKoszyk = implode(",",$Produkty);
                setcookie("koszyk".$username,$nowyKoszyk);
            }
        }else{
            setcookie("koszyk".$username,"Produkt4");
        }
    }
    ?>
</form>
<form method="post">
    <input type="submit" name="logout" value="logout">
</form>
<a href="Zad4a.php">Przejdź do koszyka</a>
</body>
</html>
