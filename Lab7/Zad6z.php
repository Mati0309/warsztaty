<?php
$r=$_POST['Rok'];
if (!empty($r)) {
    if ($r >= 1 && $r <= 1582) {
        $x = 15;
        $y = 6;
    } else if ($r >= 1583 && $r <= 1699) {
        $x = 22;
        $y = 2;
    } else if ($r >= 1700 && $r <= 1799) {
        $x = 23;
        $y = 3;
    } else if ($r >= 1800 && $r <= 1899) {
        $x = 23;
        $y = 4;
    } else if ($r >= 1900 && $r <= 2099) {
        $x = 24;
        $y = 5;
    } else if ($r >= 2000 && $r <= 2199) {
        $x = 24;
        $y = 6;
    } else {
        echo "Nieprawidłowy rok";
        return;
    }
    $a = $r % 19;
    $b = $r % 4;
    $c = $r % 7;
    $d = (19 * $a + $x) % 30;
    $f = (2*$b + 4*$c + 6*$d + $y) % 7;

    if ($f == 6 && $d == 29) {
        echo "Wielkanoc jest 26 kwietnia";
    } else if ($f == 6 && $d == 28 && ((11 * $x + 11) % 30 < 19)) {
        echo "Wielkanoc jest 18 kwietnia";
    } else if ($d + $f < 10) {
        $z = 22 + $d + $f;
        echo "Wielkanoc jest $z marca";
    } else if ($d + $f > 9) {
        $q = $d + $f - 9;
        echo "Wielkanoc jest $q kwietnia";
    }
} else {
    echo "Nie podano roku";
}

?>
