<?php
if (isset($_POST['liczba1']) && isset($_POST['liczba2'])) {
    if (is_numeric($_POST['liczba1']) && is_numeric($_POST['liczba2'])) {
        $dzialanie = $_POST['dzialanie'];
        echo "W formularzu podano liczby {$_POST['liczba1']} oraz {$_POST['liczba2']}.<br>";
        $wynik = 0;
        switch($dzialanie) {
            case 0:
                echo "Wybrano dodawanie<br>";
                $wynik = $_POST['liczba1'] + $_POST['liczba2'];
                echo "Wynik działania: {$_POST['liczba1']} + {$_POST['liczba2']} = ".$wynik;
                break;
            case 1:
                echo "Wybrano odejmowanie<br>";
                $wynik = $_POST['liczba1'] - $_POST['liczba2'];
                echo "Wynik działania: {$_POST['liczba1']} - {$_POST['liczba2']} = ".$wynik;
                break;
            case 2:
                echo "Wybrano mnożenie<br>";
                $wynik = $_POST['liczba1'] * $_POST['liczba2'];
                echo "Wynik działania: {$_POST['liczba1']} * {$_POST['liczba2']} = ".$wynik;
                break;
            case 3:
                echo "Wybrano dzielenie<br>";
                $wynik = $_POST['liczba1'] / $_POST['liczba2'];
                echo "Wynik działania: {$_POST['liczba1']} / {$_POST['liczba2']} = ".$wynik;
                break;
        }

    } else {
        echo "Błędne dane! Jedna lub obie liczby są niepoprawne!<br>";}
} else if (isset($_POST['liczba3'])) {
    if (is_numeric($_POST['liczba3'])) {
        $dzialanie = $_POST['zaawansowane'];
        echo "W formularzu podano liczbę {$_POST['liczba3']}.<br>";
        switch($dzialanie) {
            case 4:
                echo "Wybrano cosinus<br>";
                echo "Wynik działania: cos{$_POST['liczba3']}  = ".cos($_POST['liczba3']);
                break;
            case 5:
                echo "Wybrano sinus<br>";
                echo "Wynik działania: sin{$_POST['liczba3']}  = ".sin($_POST['liczba3']);
                break;
            case 6:
                echo "Wybrano tangens<br>";
                echo "Wynik działania: tan{$_POST['liczba3']}  = ".tan($_POST['liczba3']);
                break;
            case 7:
                echo "Wybrano transformacje z systemu binarnego na dziesiętny<br>";
                echo "Wynik działania: bindec{$_POST['liczba3']}  = ".bindec($_POST['liczba3']);
                break;
            case 8:
                echo "Wybrano transformacje z systemu dziesiętnego na binarny<br>";
                echo "Wynik działania: decbin{$_POST['liczba3']}  = ".decbin($_POST['liczba3']);
                break;
            case 9:
                echo "Wybrano transformacje z systemu szesnastkowego na dziesiętny<br>";
                echo "Wynik działania: hexdec{$_POST['liczba3']}  = ".hexdec($_POST['liczba3']);
                break;
            case 10:
                echo "Wybrano transformacje z systemu dziesiętnego na szesnastkowy<br>";
                echo "Wynik działania: dechex{$_POST['liczba3']}  = ".dechex($_POST['liczba3']);
                break;
        }
    } else {
        echo "Błędne dane! Liczba jest niepoprawna!<br>";
    }
} else {
    echo "Brak danych! Wymagane liczby nie zostały podane!<br>";
}
?>