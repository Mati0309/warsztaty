<?php

class InsuranceCar extends NewCar {
    private $firstOwner;
    private $years;

    public function __construct($model, $price, $exchangeRate, $alarm, $radio, $climatronic, $firstOwner, $years) {
        parent::__construct($model, $price, $exchangeRate, $alarm, $radio, $climatronic);
        $this->firstOwner = $firstOwner;
        $this->years = $years;
    }

    public function getFirstOwner() {
        return $this->firstOwner;
    }

    public function setFirstOwner($firstOwner) {
        $this->firstOwner = $firstOwner;
    }

    public function getYears() {
        return $this->years;
    }

    public function setYears($years) {
        $this->years = $years;
    }

    public function value() {
        $baseValue = parent::value();
        $discount = $baseValue * (0.01 * $this->years);
        if ($this->firstOwner) {
            $discount += $baseValue * 0.05;
        }
        return $baseValue - $discount;
    }

    public function __toString() {
        return parent::__toString() . ", First Owner: " . ($this->firstOwner ? 'true' : 'false') . ", Years: " . $this->years . ", Value in PLN with insurance: " . $this->value() . " PLN";
    }
}